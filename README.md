# Midwire Common Gem

[![Gem Version](https://badge.fury.io/rb/midwire_common.png)](https://badge.fury.io/rb/midwire_common)

**Version: 1.1.1**

A handy, light-weight Ruby library for Midwire development

## Installation

Add this line to your application's Gemfile:

    gem 'midwire_common'

And then execute:

    $ bundle install

## Usage

### Ruby Class Extensions

To use the standard class extensions you must include the individual files or 'midwire_common/all' to include everything:

    require 'midwire_common/all'

... or include individual files as necessary:

    require 'midwire_common/string'
    require 'midwire_common/array'

### Rake Tasks

To use the rake tasks simply `load` that rake file from within your main `Rakefile`:

    begin
      require 'midwire_common/rake_tasks'
    rescue Exception => e
      puts ">>> You have to run that under 'bundle exec'"
      exit
    end

Create a `CHANGELOG` file for your project.

    touch CHANGELOG

The `rake version:*` tasks will prompt for CHANGELOG entries. Simply type them in and hit <CTRL>-d on a blank line when finished.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
