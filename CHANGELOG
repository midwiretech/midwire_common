*1.1.1* (March 15, 2016)

* Rename string slicing methods left and right to left_substr and right_substr respectively to avoid any likely conflicts.

*1.1.0* (March 10, 2016)

* Fix many code smells
* Remove some duplicate methods that were provided by Ruby 2.x and stdlib

*1.0.0* (March 10, 2016)

* Remove thor dependency
* Add Ruby 2.x as the required_ruby_version
* Add version badge to README.md

*0.3.0* (March 10, 2016)

* Cleanup the code
* Refactor version.rake

*0.2.0* (March 10, 2016)

* Call `load` before returning data in YamlSetting

*0.1.18* (February 01, 2016)

* Add 'data' method for YamlSetting
* Also add 'root' method for the module.

*0.1.17* (February 01, 2016)

* Fix some redundant self references in string.rb
* Simplify the email regex in string.rb
* Add 'snakerirze' and 'camelize' methods in string.rb
* Fix `version:bump` rake task to use single-quote string for VERSION.
* Remove 'pry' dependency for 'version.rake'
* Upgrade rubocop version
* Update the README.
* Add some specs to cover changes.

*0.1.16* (October 13, 2015)

* Add BottomlessHash
* Add `thor` dependency for version bump rake tasks

*0.1.15* (March 08, 2015)

* Add YamlSetting class

*0.1.14* (March 03, 2015)

* Require 'string' for rake task
* Bump ruby version to 2.2.0

*0.1.13* (January 07, 2015)

* Upgrade to Ruby 2.x idiosyncracies, i.e., Fixnum already provides 'odd?', 'even?' methods, so I removed those, etc.
* Fix lots of Rubocop issues.

*0.1.12* (December 22, 2014)

* Add diff, apply_diff and apply_diff! instance methods for Hash

*0.1.11* (November 21, 2014)

* Fixed a bad release in 0.1.10

*0.1.10* (November 21, 2014)

* Fixed some rubocop issues
* Removed the ruby_prof dependency

*0.1.9* (September 19, 2014)

* Change space-padding for the hour to zero-padding, for the Time.timestamp method.

*0.1.8* (March 19, 2014)

* Changed String#here_with_pipe instance method to accept any delimeter instead of the boolean 'linefeeds'.

*0.1.7* (December 31, 2013)

* Added branch checking to guard against bumping version on master branch

*0.1.6* (December 31, 2013)

* Handle nested modules

*0.1.5* (September 19, 2013)

* Added #pop method to Hash
* Randomize spec runs
* Cleaned up un-needed development gems

*0.1.4* (August 19, 2013)

* Added TimeTool for time string conversions

*0.1.3* (July 29, 2013)

* Fixed bug in age method

*0.1.2* (July 29, 2013)

* Removed deprecated :rubygems source from Gemfile
* Removed deprecated :version specification from Guardfile
* Added a DataFileCache class

*0.1.1* (January 28, 2013)

* Updated README.md file with more accurate usage instructions

*0.1.0* (March 29, 2012)

* Initial Commit
