# coding: utf-8
require File.expand_path('../lib/midwire_common/version', __FILE__)

Gem::Specification.new do |spec|
  spec.name          = 'midwire_common'
  spec.version       = MidwireCommon::VERSION
  spec.authors       = ['Chris Blackburn']
  spec.email         = ['87a1779b@opayq.com']
  spec.summary       = 'Midwire Tech Ruby Library'
  spec.description   = 'A useful Ruby library'
  spec.homepage      = 'https://bitbucket.org/midwiretech/midwire_common'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^spec/})
  spec.require_paths = ['lib']

  spec.required_ruby_version = '~> 2'

  spec.add_development_dependency 'rspec', '~> 2.14'
  spec.add_development_dependency 'simplecov', '~> 0.7'
  spec.add_development_dependency 'guard', '~> 2.11'
  spec.add_development_dependency 'guard-bundler', '~> 2.1'
  spec.add_development_dependency 'guard-rspec', '~> 4.5'
  spec.add_development_dependency 'guard-rubocop', '~> 1.2'
  spec.add_development_dependency 'pry-nav', '~> 0.2'
  spec.add_development_dependency 'rake', '~> 10.1'
  spec.add_development_dependency 'rubocop', '~> 0.36'
end
