require 'pathname'
require 'midwire_common/version'

# Main module namespace
module MidwireCommon
  class << self
    def root
      Pathname.new(File.dirname(__FILE__)).parent
    end
  end

  autoload :NumberBehavior, 'midwire_common/number_behavior'
  autoload :RakeHelper,     'midwire_common/rake_helper'
end
