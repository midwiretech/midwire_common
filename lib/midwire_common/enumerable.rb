# A more useful Enumerable module
module Enumerable
  # Sort by frequency of occurrence
  def sort_by_frequency
    histogram = each_with_object(Hash.new(0)) do |elem, hash|
      hash[elem] += 1
      hash
    end
    sort_by { |elem| [histogram[elem] * -1, elem] }
  end
end
