# A light-weight super Array class
class Array
  def count_occurrences
    hash = Hash.new(0)
    each { |elem| hash[elem] += 1 }
    hash
  end

  def randomize
    sort_by { rand }
  end

  def randomize!
    replace(randomize)
  end

  def sort_case_insensitive
    sort_by(&:downcase)
  end

  def each_with_first_last(first_code, main_code, last_code)
    each_with_index do |item, ndx|
      case ndx
      when 0 then first_code.call(item)
      when size - 1 then last_code.call(item)
      else main_code.call(item)
      end
    end
  end

  # Binary search returns index if found or nil
  # rubocop:disable Metrics/LineLength
  # rubocop:disable Style/SpaceAfterComma, Style/SpaceAroundOperators, Style/SpaceAfterColon, Style/SpaceAfterSemicolon, Style/Semicolon
  def binsearch(e, l = 0, u = length - 1)
    return if l>u;m=(l+u)/2;e<self[m]?u=m-1:l=m+1;e==self[m]?m:binsearch(e,l,u)
  end
  # rubocop:enable Style/SpaceAfterComma, Style/SpaceAroundOperators, Style/SpaceAfterColon, Style/SpaceAfterSemicolon, Style/Semicolon

  # Make a string from a multi-dimensional array :
  # 1 dimension :
  # [1,2,3].superjoin(["pre-array","between-cell","post-array"])
  #
  # 2 dimensions: (html table)
  # [[1,2],[2,3]].superjoin( %w{<table><tr> </tr><tr> </tr></table>}, %w{<td> </td><td> </td>} )
  # => <table><tr><td>1</td><td>2</td></tr><tr><td>2</td><td>3</td></tr></table>
  def superjoin(*ldescr)
    dim = ldescr[0]
    rest = ldescr[1..-1]
    dim[0] + map do |arr|
      (arr.respond_to?(:superjoin) && !rest.empty?) ? arr.superjoin(*rest) : arr.to_s
    end.join(dim[1]) + dim[2]
  end
  # rubocop:enable Metrics/LineLength
end
