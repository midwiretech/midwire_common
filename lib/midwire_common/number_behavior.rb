module MidwireCommon
  # Common number behavior
  module NumberBehavior
    # Format a number with commas and a decimal point
    # rubocop:disable Style/PerlBackrefs
    def commify
      to_s =~ /([^\.]*)(\..*)?/
      int = $1.reverse
      dec = $2 ? $2 : ''
      while int.gsub!(/(,|\.|^)(\d{3})(\d)/, '\1\2,\3')
      end
      int.reverse + dec
    end
    # rubocop:enable Style/PerlBackrefs
  end
end
