class File
  # A more useful File::Stat class
  class Stat
    # Return device name for a given file
    def self.device_name(file)
      Dir['/dev/*'].inject({}) do |hash, node|
        hash.update(File.stat(node).rdev => node)
      end.values_at(File.stat(file).dev).first || nil
    end
  end
end
