# A light-weight super String class
class String
  class << self
    def random(count = 6, ranges = [('a'..'z'), ('A'..'Z'), ('0'..'9')])
      coll = ranges.map(&:to_a).flatten
      (0..(count - 1)).map { coll[rand(coll.length)] }.join
    end
  end

  def left_substr(count)
    slice(0, count)
  end

  def right_substr(count)
    slice(-count, count)
  end

  def left_trim
    # remove leading whitespace
    gsub(/^[\t\s]+/, '')
  end

  def left_trim!
    gsub!(/^[\t\s]+/, '') || ''
  end

  def right_trim
    # remove trailing whitespace
    gsub(/[\t\s]+$/, '')
  end

  def right_trim!
    gsub!(/[\t\s]+$/, '') || ''
  end

  def trim
    # remove leading and trailing whitespace
    left_trim.right_trim
  end

  def trim!
    # remove leading and trailing whitespace
    left_trim!.right_trim! || ''
  end

  # html = <<-stop.here_with_pipe(delimeter="\n")
  #   |<!-- Begin: comment  -->
  #   |<script type="text/javascript">
  # stop
  def here_with_pipe(delimeter = ' ')
    lines = split("\n")
    lines.map! { |c| c.sub!(/\s*\|/, '') }
    new_string = lines.join(delimeter)
    replace(new_string)
  end

  def alpha_numeric?
    regex = /^[a-zA-Z0-9]+$/
    (self =~ regex) == 0 ? true : false
  end

  def email_address?
    email_regex = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

    (self =~ email_regex) == 0 ? true : false
  end

  def zipcode?
    self =~ %r{^(\d{5})(-\d{4})?$}x ? true : false
  end

  def numeric?
    Float(self)
  rescue
    false # not numeric
  else
    true # numeric
  end

  def format_phone
    gsub!(/[a-z,! \-\(\)\:\;\.\&\$]+/i, '')
    '(' << slice(0..2) << ')' << slice(3..5) << '-' << slice(-4, 4)
  end

  def sanitize
    gsub(/[^a-z0-9,! \-\(\)\:\;\.\&\$]+/i, '')
  end

  def sanitize!
    gsub!(/[^a-z0-9,! \-\(\)\:\;\.\&\$]+/i, '')
  end

  def shorten(maxcount = 30)
    if length >= maxcount
      shortened = self[0, maxcount]
      splitted = shortened.split(/\s/)
      if splitted.length > 1
        words = splitted.length
        splitted[0, words - 1].join(' ') + '...'
      else
        shortened[0, maxcount - 3] + '...'
      end
    else
      self
    end
  end

  def escape_single_quotes
    gsub(/[']/, '\\\\\'')
  end

  def escape_double_quotes
    gsub(/["]/, '\\\\\"')
  end

  def snakerize
    gsub(/::/, '/')
        .gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
        .gsub(/([a-z\d])([A-Z])/, '\1_\2')
        .tr('-', '_')
        .downcase
  end

  # rubocop:disable Style/PerlBackrefs
  def camelize
    gsub(/\/(.?)/) { '::' + $1.upcase }.gsub(/(^|_)(.)/) { $2.upcase }
  end
end
