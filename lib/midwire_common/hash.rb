# By Nick Ostrovsky
# http://firedev.com/posts/2015/bottomless-ruby-hash/
class BottomlessHash < Hash
  def initialize
    super(&-> (hash, key) { hash[key] = self.class.new })
  end

  def self.from_hash(hash)
    new.merge(hash)
  end
end

# A more useful Hash class
class Hash
  def bottomless
    BottomlessHash.from_hash(self)
  end

  # A better grep
  def grep(pattern)
    each_with_object([]) do |kv, res|
      res << kv if kv[0] =~ pattern || kv[1] =~ pattern
      res
    end
  end

  # rubocop:disable Metrics/AbcSize
  def diff(other)
    (keys + other.keys).uniq.each_with_object({}) do |key, memo|
      unless self[key] == other[key]
        memo[key] = if self[key].is_a?(Hash) && other[key].is_a?(Hash)
                      self[key].diff(other[key])
                    else
                      [self[key], other[key]]
                    end
      end
      memo
    end
  end
  # rubocop:enable Metrics/AbcSize

  # TODO: Spec/Test this method
  def apply_diff!(changes, direction = :right)
    path = [[self, changes]]
    pos, local_changes = path.pop
    while local_changes
      local_changes.each_pair do |key, change|
        if change.is_a?(Array)
          pos[key] = (direction == :right) ? change[1] : change[0]
        else
          path.push([pos[key], change])
        end
      end
      pos, local_changes = path.pop
    end
    self
  end

  # TODO: Spec/Test this method
  # rubocop:disable Metrics/AbcSize
  def apply_diff(changes, direction = :right)
    cloned = clone
    path = [[cloned, changes]]
    pos, local_changes = path.pop
    while local_changes
      local_changes.each_pair do |key, change|
        if change.is_a?(Array)
          pos[key] = (direction == :right) ? change[1] : change[0]
        else
          pos[key] = pos[key].clone
          path.push([pos[key], change])
        end
      end
      pos, local_changes = path.pop
    end
    cloned
  end
  # rubocop:enable Metrics/AbcSize

  # Usage { :a => 1, :b => 2, :c => 3}.except(:a) -> { :b => 2, :c => 3}
  def except(*keys)
    reject { |key, _v| keys.flatten.include?(key.to_sym) }
  end

  # Usage { :a => 1, :b => 2, :c => 3}.only(:a) -> {:a => 1}
  def only(*keys)
    dup.reject { |key, _v| !keys.flatten.include?(key.to_sym) }
  end

  # Usage h = { :a => 1, :b => 2, :c => 3}.pop(:a) -> {:a => 1}
  # ... and now h == { :b => 2, :c => 3}
  def pop(*keys)
    ret = reject { |key, _v| !keys.flatten.include?(key.to_sym) }
    reject! { |key, _v| keys.flatten.include?(key.to_sym) }
    ret
  end

  # Usage { :a => 1, :b => 2, :c => 3}.to_query_string #= a=1&b=2&c=3
  def to_query_string
    require 'uri'
    map do |key, value|
      "#{URI.encode(key.to_s)}=#{URI.encode(value.to_s)}"
    end.join('&')
  end

  # rubocop:disable Style/RescueModifier
  def symbolize_keys!
    keys.each do |key|
      self[(key.to_sym rescue key) || key] = delete(key)
    end
    self
  end
  # rubocop:enable Style/RescueModifier

  def symbolize_keys
    dup.symbolize_keys!
  end

  # Recursively change keys to symbols
  # Modifies the hash keys in place.
  def recursively_symbolize_keys!
    symbolize_keys!
    values.each do |value|
      value.recursively_symbolize_keys! if value.is_a?(Hash)
    end
    self
  end
end
