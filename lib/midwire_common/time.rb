# A more useful Time class
class Time
  class << self
    def timestamp(resolution = 3)
      return Time.now.strftime('%Y%m%d%H%M%S') if resolution < 1
      Time.now.strftime("%Y%m%d%H%M%S.%#{resolution}N")
    end
  end
end
