module MidwireCommon
  # A simple class to cache data in a file
  class DataFileCache
    def initialize(filename)
      @cache_dir  = File.dirname(filename)
      @cache_file = normalize_filename(filename)
      ensure_cache_dir
    end

    def put(data)
      newdata = data.dup
      newdata = newdata.join("\n") if newdata.is_a? Array
      File.open(@cache_file, 'w') { |file| file.write(newdata) }
    end

    def get
      File.read(@cache_file)
    end

    def age
      return 999_999_99.0 unless File.exist?(@cache_file)
      (Time.now - File.ctime(@cache_file)).to_f
    end

    private

    def ensure_cache_dir
      FileUtils.mkdir_p(@cache_dir) unless File.exist?(@cache_dir)
    end

    def normalize_filename(filename)
      unless filename.match(Regexp.new(@cache_dir))
        filename = "#{@cache_dir}/#{filename}"
      end
      filename
    end
  end
end
