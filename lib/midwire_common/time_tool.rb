module MidwireCommon
  # A useful mixing for Time behavior
  class TimeTool
    class << self
      # converts the given time (HH:MM:SS) to seconds
      #
      # +time+ the time-string
      def time_to_seconds(time)
        return -1 if time.nil? || time.strip.empty?
        times = time.split(/:/).reverse
        seconds = 0
        (0...times.length).each_with_index do |int|
          seconds += times[int].to_i * (60**int)
        end
        seconds
      end

      # converts the given seconds into a time string (HH:MM:SS)
      #
      # +seconds+ the seconds to convert
      def seconds_to_time(seconds)
        return 'unknown' if seconds.nil?
        tsec = seconds
        time = ''
        2.downto(0) do |int|
          power = to_the_power(60, int)
          tmp = tsec / power
          tsec -= tmp * power
          time += ':' unless time.empty?
          time += format('%02d', tmp)
        end
        time
      end

      private

      def to_the_power(base, expon)
        base**expon
      end
    end
  end
end
