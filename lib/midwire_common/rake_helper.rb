require 'midwire_common'

module MidwireCommon
  # RakeHelper helps to automate gem release and versioning tasks
  class RakeHelper
    include Rake::DSL if defined? Rake::DSL

    def self.install_tasks(opts = {})
      dir = opts[:dir] || Dir.pwd
      new(dir).install
    end

    attr_reader :basedir

    def initialize(basedir)
      @basedir = basedir
    end

    def install
      task_dir = File.expand_path('../tasks', File.dirname(__FILE__))
      Dir["#{task_dir}/*.rake"].sort.each { |ext| load ext }
    end
  end
end
