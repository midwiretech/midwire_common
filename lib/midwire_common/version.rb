original_verbosity = $VERBOSE
$VERBOSE = nil
module MidwireCommon
  VERSION = '1.1.1'.freeze
end
$VERBOSE = original_verbosity
