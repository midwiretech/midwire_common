if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start do
    add_filter 'spec/'
    add_filter 'vendor/'
  end
end

require 'pry'
require File.join(File.dirname(__FILE__), '..', 'lib', 'midwire_common')
require 'midwire_common/all'

PROJECT_ROOT = File.expand_path('..', File.dirname(__FILE__))

RSpec.configure do |config|
  include MidwireCommon

  config.mock_with :rspec
  config.color = true
  config.order = 'random'

  def capture(stream)
    begin
      stream = stream.to_s
      eval "$#{stream} = StringIO.new"
      yield
      result = eval("$#{stream}").string
    ensure
      eval("$#{stream} = #{stream.upcase}")
    end

    result
  end
end
