require 'spec_helper'

describe Fixnum do
  it 'knows if it is odd or even' do
    777.odd?.should eq(true)
    776.odd?.should eq(false)

    777.even?.should eq(false)
    776.even?.should eq(true)
  end

  it 'can format itself with commas' do
    8_729_928_827.commify.should == '8,729,928,827'
  end
end
