require 'spec_helper'

describe Float do
  it 'can format itself with commas' do
    8_729_928_827.0.commify.should eq('8,729,928,827.0')
    8_729_928_827.20332002.commify.should eq('8,729,928,827.20332')
  end
end
