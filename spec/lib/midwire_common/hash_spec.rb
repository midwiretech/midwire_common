require 'spec_helper'

describe BottomlessHash do
  subject { described_class.new }

  it 'does not raise on missing key' do
    expect do
      subject[:missing][:key]
    end.to_not raise_error
  end

  it 'returns an empty value on missing key' do
    expect(subject[:missing][:key]).to be_empty
  end

  it 'stores and returns keys' do
    subject[:existing][:key] = :value
    expect(subject[:existing][:key]).to eq(:value)
  end

  context '#from_hash' do
    let(:hash) do
      { existing: { key: { value: :hello } } }
    end

    subject do
      described_class.from_hash(hash)
    end

    it 'returns old hash values' do
      expect(subject[:existing][:key][:value]).to eq(:hello)
    end

    it 'provides a bottomless version' do
      expect(subject[:missing][:key]).to be_empty
    end

    it 'stores and returns new values' do
      subject[:existing][:key] = :value
      expect(subject[:existing][:key]).to eq(:value)
    end

    it 'converts nested hashes as well' do
      expect do
        subject[:existing][:key][:missing]
      end.to_not raise_error
    end
  end
end

describe Hash do
  it 'greps key/value pairs using a regular expression' do
    h = { a: 'this is a test', 'b' => 'this is not the answer' }
    expect(h.grep(/a test/)).to eq([[:a, 'this is a test']])
    expect(h.grep(/b/)).to eq([['b', 'this is not the answer']])
    expect(
      h.grep(/^this/)
    ).to eq([[:a, 'this is a test'], ['b', 'this is not the answer']])
  end

  it 'returns elements with certain keys filtered out' do
    expect({ a: 1, b: 2, c: 3 }.except(:a)).to eq(b: 2, c: 3)
  end

  it 'returns elements for discretely passed keys' do
    expect({ a: 1, b: 2, c: 3 }.only(:a)).to eq(a: 1)
  end

  it 'pops an element off of the stack' do
    h = { a: 1, b: 2, c: 3 }
    expect(h.pop(:a)).to eq(a: 1)
    expect(h).to eq(b: 2, c: 3)
  end

  it 'returns a query string' do
    expect({ a: 1, b: 2, c: 3 }.to_query_string).to eq('a=1&b=2&c=3')
  end

  it 'symbolizes its keys' do
    h = { 'a' => 1, 'b' => 2, 'c' => 3 }
    expect(h.symbolize_keys).to eq(a: 1, b: 2, c: 3)
    h.symbolize_keys!
    expect(h).to eq(a: 1, b: 2, c: 3)
  end

  it 'recursively symbolizes its keys' do
    h = {
      'a' => 1,
      'b' => {
        'a' => 1,
        'b' => 2,
        'c' => {
          'a' => 1,
          'b' => 2,
          'c' => 3
        }
      },
      'c' => 3
    }
    expect(h.recursively_symbolize_keys!).to eq(
      a: 1, b: { a: 1, b: 2, c: { a: 1, b: 2, c: 3 } }, c: 3
    )
  end

  context 'diff methods' do
    let(:h1_keys) { { 'a' => 1, 'b' => 2, 'c' => 3 } }
    let(:h2_keys) { { 'a' => 1, 'b' => 2, 'd' => 3 } }

    let(:h1_keys_nested) { { 'a' => 1, 'b' => 2, { 'x' => 99 } => 3 } }
    let(:h2_keys_nested) { { 'a' => 1, 'b' => 2, { 'x' => 99 } => 4 } }

    let(:h1_values) { { 'a' => 1, 'b' => 2, 'c' => 3 } }
    let(:h2_values) { { 'a' => 1, 'b' => 2, 'c' => 4 } }

    context '.diff' do
      it 'reports different keys' do
        expect(h1_keys.diff(h2_keys)).to eq('c' => [3, nil], 'd' => [nil, 3])
      end

      it 'reports different values' do
        expect(h1_values.diff(h2_values)).to eq('c' => [3, 4])
      end

      it 'reports different keys even with nested hashes' do
        expect(h1_keys_nested.diff(h2_keys_nested)).to eq(
          { 'x' => 99 } => [3, 4]
        )
      end
    end
  end
end
