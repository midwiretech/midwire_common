require 'spec_helper'

describe MidwireCommon::DataFileCache do
  let(:cache_dir) { '/tmp/.cache_dir' }
  let(:filename) { 'data_file_cache' }
  let(:filepath) { "#{cache_dir}/#{filename}" }
  let(:data) { 'this is my data' }

  before(:each) do
    @cache = MidwireCommon::DataFileCache.new("#{cache_dir}/#{filename}")
  end

  it 'creates a directory for the cache file' do
    File.exist?(cache_dir).should eq(true)
  end

  it 'caches data' do
    @cache.put(data)
    File.exist?(filepath).should eq(true)
    x = File.read(filepath)
    x.should == data
  end

  it 'determines the age of the cached data' do
    tm = @cache.age
    (tm > 0).should eq(true)
  end

  it 'retrieves cached data' do
    dta = @cache.get
    dta.should == data
  end

  it 'prepends the filename with cache dir' do
    testfile = 'testfile'
    nf = @cache.send(:normalize_filename, testfile)
    nf.should == "#{cache_dir}/#{testfile}"
  end
end
