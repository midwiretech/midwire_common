require 'spec_helper'

describe File::Stat do
  it 'knows on which devise it resides' do
    dev = File::Stat.device_name('/tmp')
    dev.should_not be_nil
  end
end
