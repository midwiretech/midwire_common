require 'spec_helper'

module MidwireCommon
  RSpec.describe RakeHelper do
    let(:rake_helper) { described_class.new(Dir.pwd) }

    context '#new' do
      it 'returns an instance' do
        expect(rake_helper).to be_a(described_class)
      end

      it 'sets @basedir' do
        expect(rake_helper.basedir).to eq(Dir.pwd)
      end
    end

    context '.install' do
      it 'returns version.rake' do
        file = rake_helper.install.first
        expect(File.basename(file)).to eq('version.rake')
      end

      it 'loads version.rake' do
        file = File.join(MidwireCommon.root, 'lib/tasks/version.rake')
        expect(rake_helper).to receive(:load).with(file)
        rake_helper.install
      end
    end
  end
end
