require 'spec_helper'

describe MidwireCommon::YamlSetting do
  let(:root) { MidwireCommon.root }
  let(:tmpdir) { File.join(root, 'tmp') }
  let(:file) { File.join(tmpdir, 'bogus.yml') }
  let(:setting) { YamlSetting.new(file) }

  before do
    FileUtils.touch(file)
    setting[:test] = {}
    setting[:test][:a] = 1
    setting[:test][:b] = 2
    setting[:test][:c] = 3
    setting.save
  end

  context '#new' do
    it 'sets the :file accessor' do
      expect(setting.file).to eq(file)
    end
  end

  context '.load' do
    it 'returns self' do
      expect(setting.load).to be_a(YamlSetting)
    end

    it 'loads the YAML' do
      setting.load
      expect(setting[:test]).to be_a(Hash)
      expect(setting[:test][:a]).to eq(1)
      expect(setting[:test][:b]).to eq(2)
      expect(setting[:test][:c]).to eq(3)
    end
  end

  context '.save' do
    it 'stores the current hash' do
      setting[:test] = 'bogus'
      setting.save
      expect(setting[:test]).to eq('bogus')
    end
  end

  context '.config' do
    it 'returns the data hash' do
      expect(setting.config[:test]).to eq(a: 1, b: 2, c: 3)
    end
  end

  context '.data' do
    it 'returns the data hash' do
      expect(setting.data[:test]).to eq(a: 1, b: 2, c: 3)
    end
  end
end
