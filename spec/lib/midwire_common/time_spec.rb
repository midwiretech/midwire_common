require 'spec_helper'

describe Time do
  context 'generates a timestamp' do
    it 'appropriate for a filename' do
      ts = Time.timestamp
      expect(ts.length).to eq(18)
      expect(ts.numeric?).to eq(true)
    end

    it 'with the only seconds' do
      ts = Time.timestamp(0)
      expect(ts.length).to eq(14)
      expect(ts.numeric?).to eq(true)
    end

    it 'defaults to nanosecond resolution' do
      ts = Time.timestamp
      expect(ts.length).to eq(18)
      expect(ts.numeric?).to eq(true)
    end

    it 'with the millisecond resolution' do
      ts = Time.timestamp(3)
      expect(ts.length).to eq(18)
      expect(ts.numeric?).to eq(true)
    end

    it 'with the microsecond resolution' do
      ts = Time.timestamp(6)
      expect(ts.length).to eq(21)
      expect(ts.numeric?).to eq(true)
    end

    it 'with the nanosecond resolution' do
      ts = Time.timestamp(9)
      expect(ts.length).to eq(24)
      expect(ts.numeric?).to eq(true)
    end

    it 'with the picosecond resolution' do
      ts = Time.timestamp(12)
      expect(ts.length).to eq(27)
      expect(ts.numeric?).to eq(true)
    end
  end
end
