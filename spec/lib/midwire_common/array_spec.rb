require 'spec_helper'

describe Array do
  it 'counts occurrences of an element' do
    [1, 2, 2, 3, 3, 3].count_occurrences.should eq(1 => 1, 2 => 2, 3 => 3)
    %w(asdf asdf qwer asdf).count_occurrences.should == {
      'asdf' => 3,
      'qwer' => 1
    }
  end

  it 'randomizes the order of an array' do
    myarray = [1, 2, 3, 4, 5, 6, 7, 8, 9, '0']
    myarray.randomize.should_not eq(myarray)
    myarray.randomize!
    myarray.should_not == [1, 2, 3, 4, 5, 6, 7, 8, 9, '0']
  end

  it 'sorts elements case insensitive' do
    myarray = %w(zebra ghost Zebra cat Cat)
    myarray.sort_case_insensitive.should eq(%w(Cat cat ghost Zebra zebra))
  end

  it 'can process first and last entries differently than others' do
    text = ''
    ['KU', 'K-State', 'MU'].each_with_first_last(
      lambda do |team|
        text += "#{team} came first in the NCAA basketball tournament.\n"
      end,
      lambda do |team|
        text += "#{team} did not come first or last in the final four.\n"
      end,
      lambda do |team|
        text += "#{team} came last in the final four this year.\n"
      end
    )
    text.should == <<-string.here_with_pipe("\n")
      |KU came first in the NCAA basketball tournament.
      |K-State did not come first or last in the final four.
      |MU came last in the final four this year.
      |
    string
  end

  it 'can binary search for elements' do
    a = %w(a b c)
    a.binsearch('a').should eq(0)
    a.binsearch('b').should eq(1)
    a.binsearch('c').should eq(2)
  end

  it 'can superjoin elements' do
    [1, 2, 3].superjoin(['->', '+', '<-']).should eq('->1+2+3<-')
    [[1, 2], [2, 3]].superjoin(
      %w(<table><tr> </tr><tr> </tr></table>), %w(<td> </td><td> </td>)
    ).should eq(
      '<table><tr><td>1</td><td>2</td></tr><tr><td>2</td><td>3</td></tr></table>'
    )
  end
end
