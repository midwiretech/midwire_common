# encoding: utf-8

# rubocop:disable Metrics/LineLength
require 'spec_helper'

RSpec.describe String do
  it 'is a String' do
    ''.should be_a String
  end

  it 'generates a random string' do
    String.random.length.should == 6
  end

  context 'slicing methods' do
    it "'left' returns the leftmost 'n' characters" do
      'My Bogus String'.left_substr(2).should == 'My'
    end

    it "'right' returns the rightmost 'n' characters " do
      'My Bogus String'.right_substr(2).should == 'ng'
    end
  end

  context 'trim method' do
    it "'left_trim' removes all whitespace from the left of the string" do
      " \t = this is a string".left_trim.should == '= this is a string'
    end

    it "'left_trim!' removes all whitespace from the left of the string" do
      mystring = " \t \t a test is coming"
      mystring.left_trim!
      mystring.should == 'a test is coming'
    end

    it "'right_trim' removes all whitespace from the right of the string" do
      "= this is a string \t ".right_trim.should == '= this is a string'
    end

    it "'right_trim!' removes all whitespace from the right of the string" do
      mystring = " \t \t a test is coming \t \t "
      mystring.right_trim!
      mystring.should == " \t \t a test is coming"
    end

    it "'trim' removes whitespace from both sides of the string" do
      " \t \t a test is coming \t \t ".trim.should == 'a test is coming'
    end

    it "'trim!' removes whitespace from both sides of the string" do
      mystring = " \t \t a test is coming \t \t "
      mystring.trim!
      mystring.should == 'a test is coming'
    end
  end

  context 'formatting and manipulation' do
    it 'here_with_pipe - without linefeeds' do
      html = <<-STOP.here_with_pipe
        |<!-- Begin: comment  -->
        |<script type="text/javascript">
        |</script>
      STOP
      html.should == '<!-- Begin: comment  --> <script type="text/javascript"> </script>'
    end

    it 'here_with_pipe - with linefeeds' do
      html = <<-STOP.here_with_pipe("\n")
        |<!-- Begin: comment  -->
        |<script type="text/javascript">
        |</script>
      STOP
      html.should == "<!-- Begin: comment  -->\n<script type=\"text/javascript\">\n</script>"
    end

    it 'here_with_pipe - with no space delimeter' do
      html = <<-STOP.here_with_pipe('')
        |<!-- Begin: comment  -->
        |<script type="text/javascript">
        |</script>
      STOP
      html.should == '<!-- Begin: comment  --><script type="text/javascript"></script>'
    end

    it 'format_phone returns a formatted phone number string' do
      expect('9132329999'.format_phone).to eq('(913)232-9999')
      expect('913.232.9999'.format_phone).to eq('(913)232-9999')
      expect('913 232 9999'.format_phone).to eq('(913)232-9999')
      expect('913-232-9999'.format_phone).to eq('(913)232-9999')
    end

    it 'sanitizes itself' do
      expect('|bogus|'.sanitize).to eq('bogus')
      expect('|∫|ß'.sanitize).to eq('')
      expect('ßogus'.sanitize).to eq('ogus')
      expect('<tag>bogus</tag>'.sanitize).to eq('tagbogustag')
      expect('<tag>.bogus.</tag>'.sanitize).to eq('tag.bogus.tag')
      s = '|∫|ß'
      s.sanitize!
      s.should == ''
    end

    it 'shortens itself with elipses at the end' do
      s = 'this is my very long string which I will eventually shorten with the enhanced String class that we are now testing.'
      short = s.shorten
      expect(short).to eq('this is my very long string...')
      expect(short.length).to eq(30)

      s = '1234567890123456789012345678901234567890'
      short = s.shorten
      expect(short).to eq('123456789012345678901234567...')
      expect(short.length).to eq(30)

      s = '12345678901234567890'
      short = s.shorten
      expect(short).to eq('12345678901234567890')
      expect(short.length).to eq(20)
    end

    context 'quotes' do
      it 'escapes single quotes' do
        "this is a 'test'".escape_single_quotes.should == "this is a \\'test\\'"
      end

      it 'escapes double quotes' do
        expect('this is a "test"'.escape_double_quotes)
            .to eq('this is a \\\\"test\\\\"')
      end
    end
  end

  context 'characterization' do
    it 'knows if it is alpha-numeric or not' do
      'abcd-9191'.alpha_numeric?.should eq(false)
      'abcd.9191'.alpha_numeric?.should eq(false)
      'abcd91910'.alpha_numeric?.should eq(true)
      'abcd_9191'.alpha_numeric?.should eq(false)
      'abcd 9191'.alpha_numeric?.should eq(false)
    end

    it 'knows if it is an email address or not' do
      'abcd_9191'.email_address?.should eq(false)
      'abcd@9191'.email_address?.should eq(false)
      'abcd@9191.info'.email_address?.should eq(true)
      'abcd-asdf@9191.com'.email_address?.should eq(true)
      'abcd_asdf@9191.com'.email_address?.should eq(true)
      'abcd.asdf@9191.com'.email_address?.should eq(true)
    end

    it 'knows if it is a zipcode or not' do
      '13922-2356'.zipcode?.should eq(true)
      '13922.2343'.zipcode?.should eq(false)
      '13922 2342'.zipcode?.should eq(false)
      'ABSSD'.zipcode?.should eq(false)
      'i3323'.zipcode?.should eq(false)
      '13922'.zipcode?.should eq(true)
    end

    it 'knows if it is numeric or not' do
      '12341'.numeric?.should eq(true)
      '12341.23'.numeric?.should eq(true)
      '12341.00000000000000023'.numeric?.should eq(true)
      '0.12341'.numeric?.should eq(true)
      '0x2E'.numeric?.should eq(true)
      ' 0.12341'.numeric?.should eq(true)
      ' 0.12341 '.numeric?.should eq(true)
      '.12341'.numeric?.should eq(true)
      ' 12341.'.numeric?.should eq(false)
      ' 12341.  '.numeric?.should eq(false)
    end
  end

  context '.snakerize' do
    it 'changes CamelCased string to snake_cased' do
      expect('CamelCased'.snakerize).to eq('camel_cased')
    end

    it 'handles doulbe-colon' do
      expect('Camel::CasedTwo'.snakerize).to eq('camel/cased_two')
    end
  end

  context '.camelize' do
    it 'changes snake cased string to camelized' do
      expect('camel_cased'.camelize).to eq('CamelCased')
    end

    it 'handles slash' do
      expect('camel/cased_two'.camelize).to eq('Camel::CasedTwo')
    end
  end
end
# rubocop:enable Metrics/LineLength
