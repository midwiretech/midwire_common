require 'spec_helper'

describe Enumerable do
  it 'can sort by frequency of occurrences' do
    [1, 2, 3, 3, 3, 3, 2].sort_by_frequency.should eq([3, 3, 3, 3, 2, 2, 1])
    %w(a b c d e f a f f b f a).sort_by_frequency
                               .should eq(%w(f f f f a a a b b c d e))
  end
end
