require 'spec_helper'

describe MidwireCommon::TimeTool do
  it 'converts seconds to timestamp' do
    MidwireCommon::TimeTool.seconds_to_time(92_353).should == '25:39:13'
  end

  it 'converts timestamp to seconds' do
    MidwireCommon::TimeTool.time_to_seconds('25:39:13').should == 92_353
  end
end
